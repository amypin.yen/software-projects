package ss.labs.pokemon;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        //TODO declare and initiate a Scanner
        Scanner myscanner = new Scanner(System.in);
        //TODO declare and initiate pokedex as an ArrayList to store pokemon data
        ArrayList<PokemonData> pokedex = new ArrayList<>();
        //TODO declare and initiate typeCountMap as a HashMap to count the number of each types of pokemon
        HashMap<String,Integer> typeCountMap = new HashMap<>();
        System.out.println("Enter the Number of pokemon in the pokedex:");
        //TODO 0: scan the number of the testData from user input
        int pokenum = myscanner.nextInt();
        //TODO use a loop to read several input data
        for(int i=0 ; i <pokenum;i++)
        {
            //System.out.println("Enter pokemon name:");
            //TODO 1-1:scan pokemon name and save to a variable from user input
            String pokename = myscanner.next();
            //System.out.println("Enter pokemon type:");
            //TODO 1-2:scan pokemon type and save to a variable
            String poketype = myscanner.next();
            //System.out.println("Enter pokemon's move name:");
            //TODO 1-3:scan pokemon move's name
            String pokemove = myscanner.next();
            //TODO 2-1:create a new pokemon data from the above variables
            PokemonData pokemon = new PokemonData(pokename,poketype,pokemove);
            //TODO 2-2:add the pokemon data into pokedex
            pokedex.add(pokemon);
            /*//TODO 4: Tricky part!! get the value from the type counting map , add 1 and put it back (multiple line
            if (!typeCountMap.containsKey(poketype)) {
                typeCountMap.put(poketype, 1);
            }else{
                Integer number = typeCountMap.get(poketype);
                typeCountMap.put(poketype,number+1);
            }*/
        }

        /*//TODO 5 print out the created pokemon information (multiple lines
            for(int i =0 ;i <pokenum;i++){
                PokemonData poke = pokedex.get(i);
                System.out.println("name: " + poke.name);
                System.out.println("type: " + poke.type);
                System.out.println("move " + poke.move);
            }
        //TODO 6 print out how many type of pokemons in the pokedex
            System.out.println("number of types of pokemon : " +typeCountMap.size());*/

        /******Assignmeny1******/
        Random random = new Random();
        Integer score = new Integer(0);

        System.out.println("Game on!");
        while(true){
            System.out.println("Walking......");
            //Randomly choose a pokemom
            Integer chosen = random.nextInt(pokenum);
            PokemonData chosenpoke = pokedex.get(chosen);
            //Print out pokemon info
            System.out.print("Pokemon Encountered!\n"+"name : "+ chosenpoke.name+"  type : "+chosenpoke.type+"  move :"+chosenpoke.move+"\n");

            System.out.print("Enter your reaction :");
            String cmd = myscanner.next();
            //Go through  the possible command and execute
            switch (cmd){
                case "catch": // If  the command is to catch
                    if (!typeCountMap.containsKey(chosenpoke.type)) {
                        typeCountMap.put(chosenpoke.type, 1);
                    }else{
                        Integer number = typeCountMap.get(chosenpoke.type);
                        typeCountMap.put(chosenpoke.type,number+1);
                    }
                    if(chosenpoke.type.toLowerCase().equals("grass")) score = score+1;
                    else score = score -1;
                    System.out.println("Catch "+chosenpoke.name+"!  Congrats!");
                    break;
                case  "run": // If  the command is to run
                    if(chosenpoke.type.toLowerCase().equals("grass")) score = score-1;
                    else score = score +1;
                    System.out.println("Run away successfully!");
                    break;
                case "quit": // If  the command is to quit
                    System.out.println("Quit the game......");
                    System.out.println("Your score : "+score);
                    //Iterate tyoeCountMap to print out number of types of pokemon
                    for (String key :typeCountMap.keySet()){
                        System.out.println("Number of "+ key+" type :" + typeCountMap.get(key));
                    }
                    break;
                default: // If the command doex not exist then the pokemon run away
                    System.out.print("No such commands available. Next time!");
            }
            //Quit the game
            if (cmd.equals("quit"))
                break;
        }
    }
}
