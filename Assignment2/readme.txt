104062322 嚴心平

實作
	總共有25個角色(不包括玩家)
	Villagers(*7)
		Name 		Job		Quote
		Michael		miller		Hi! How are you!
		Beck		chief		Hi! How are you!
		Angelica	banker		Hi! How are you!
        	Elizabeth	farmer		Hi! How are you!
        	Melissa		teacher		Hi! How are you!
        	Lee		musician	Hi! How are you!
        	Luara		comedian	Hi! How are you!
	Pokemon Trainer(*7)
		Name		Pokemon		Quote
	        Hillary		Pikachu		Let's battle!!!
        	Lin		Magikarp	Let's battle!!!
		Chris		Charmander	Let's battle!!!
		Bill		Squirtle	Let's battle!!!
		Kristen		Chikorita	Let's battle!!!
		Kate		Growlithe	Let's battle!!!
		George		Onix		Let's battle!!!
	Nurse
        	Joy	Can I help you? (*6個)
	Gym Leader(*5)
		Name 		Badge		Pokemons		Quote
		Morty		Fog		Gasty	Gengar		Good of you to have come here, in Ecruteak.
 		Pryce		Glacier		Seel	Dewgong		I have been with Pokémon since before you were born. I do not lose easily.
		Clair		Rising		GyaradosCharizard	I am Clair. The world's best dragon master. I can hold my own against even the Pokémon League's Elite Four. Do you still want to take me on? ...Fine. Let's do it! 
		Wattson		Dynamo		Voltorb	Magnemite	Now, what are you doing here? What's that? You say you've gotten past all my rigged doors? Wahahahah! Now, that is amusing!
		Roark		Coal		Onix	Cranidos	Welcome! This is the Oreburgh Pokémon Gym! I'm Roark, the Gym Leader! I'm but one Trainer who decided to walk proudly with Rock-type Pokémon!
	Gym leaders 與 pokemons 的資料皆採自 pokemon wiki

	角色介紹 (沿用lab2 practice 的模板)
	Player extends PokemonTrainer
		Integer  money -->擁有的錢
		HashMap<String, Integer>  badges  -->玩家贏得的徽章及數量
	
		changeMoney(Integer change)  -->玩家得到金幣 或 輸光錢幣(當change==0)
		acceptbadge(String badgename) -->將徽章加入HashMap badges中

	Gym Leader extends NPC
 		ArrayList<PokemonData> pokedex -->Gym leader 擁有的pokemons
		String badge --> Gym leader 給出的徽章名
	
		interact(Player player) -->與玩家戰鬥(在code 中有comment 詳細說明),贏了會得到100~150不等金幣

	Pokemon Trainer extends NPC
		PokemonData pokemonData --> trainer 擁有的pokemon
	
		interact(Player player) -->與玩家戰鬥(在code 中有comment 詳細說明),贏了會得到50~110不等金幣

	Nurse extends NPC
		interact(Player player) -->charge pokemon's hp

	Villagers extends NPC
		interact(Player player) -->talks and gives money

	PokemonData
		與practice不同，這部分將 id 刪除因為並沒有用到

		damaged(Integer damage)  -->受到攻擊，減少hp
		chargeHP()  -->將 hp補滿
	
	其他細節	
		如果輸入的指令不是talk ignore quit 之一，遊戲會ignore
		當pokemon的速度相等，玩家擁有先進攻權
		遇到 Gym leaders 大多贏的機率是1/2，其中有一定會輸和一定會贏的例外
