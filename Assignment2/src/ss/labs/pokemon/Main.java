package ss.labs.pokemon;
import ss.labs.pokemon.character.NPC;
import ss.labs.pokemon.character.NPCGenerator;
import ss.labs.pokemon.character.Player;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();
        PokemonData poke = new PokemonData("Bulbsoar","Grass","Whip",200,85,92);
        Player player = new Player("Morris","Pokemon Trainer","Let's battle!",poke);
	    //TODO create a character generator
        NPCGenerator getNPCs = new NPCGenerator();
        //TODO get all characters from character generator
        ArrayList<NPC> charactors = getNPCs.getNpcTemplates();
        //TODO print out characters' information, you can choose to use toString or showDescription
        while(true){
            NPC encounter = charactors.get(random.nextInt(25));
            //Print out the info of the charactor you met
            encounter.showInformation();
            System.out.print("Talk or ignore or quit?");
            String cmd = scanner.next();
            if(cmd.equals("talk")){
                //Player interacts with charactors
                encounter.interact(player);
                if (player.pokemonhp()<1){
                    //Player lost the battle, automaticlaly goes to  pokemon center to recharge pokemon's hp
                    System.out.println("Go to pokemon center......");
                    //Interact with a nurse
                    charactors.get(0).interact(player);
                }
            }
            else if (cmd.equals("quit")){
                System.out.println("Your money :"+player.getmoney());// Show player's money
                player.showBadge();// show Player's Badges
                break;
            }
            //If input " ignore" or other command, it will be ignored
        }
    }
}
