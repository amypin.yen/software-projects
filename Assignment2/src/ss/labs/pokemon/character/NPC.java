package ss.labs.pokemon.character;

/**
 * Created by jerry on 2017/3/7.
 */
public class NPC extends AbstractCharacter {

    //TODO more attributes are needed
    private String quote;
    //TODO solve the error
    public NPC(String name,String job,String quote){
        //TODO call super constructor
        super(name,job);
        //TODO assign attribute
        this.quote = quote;
    }

    public void interact(Player player)
    {
        speak();
    }

    public void speak(){
        //TODO NPC say something...
        System.out.println(name+" : "+quote);
    }
    @Override
    public void showInformation() {
        System.out.println("You meet "+name+", who is a "+job);
    }

    //TODO override toString()
    public String toString(){
        return super.toString()+quote;
    }
}
