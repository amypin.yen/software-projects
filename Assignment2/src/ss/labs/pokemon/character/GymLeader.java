package ss.labs.pokemon.character;
import ss.labs.pokemon.PokemonData;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

/**
 * Created by yen ping on 2017/3/14.
 */
public class GymLeader extends NPC{
    private ArrayList<PokemonData> pokedex = new ArrayList<>();//Gym leaders use pokedex to store thier pokemonData
    private String badge;//The badge's name, which belong to the gym leader,

    public GymLeader (String name,String job,String quote, PokemonData poke1, PokemonData poke2,String badge){
        super(name,job,quote);
        this.pokedex.add(poke1);
        this.pokedex.add(poke2);
        this.badge = badge;
    }

    public void interact(Player player){
        Random random = new Random();
        Scanner scanner = new Scanner(System.in);
        //Pick one pokemon from pokedex to fight against player
        PokemonData fight = this.pokedex.get(random.nextInt(2));
        //If attack is true   Gym Leader go first
        //else player go first
        boolean attack = (fight.getSpeed()>player.pokemonData.getSpeed());

        System.out.println("Let the battle begin......");
        while(true){
            //Push Enter to continue the game
            scanner.nextLine();
            if(attack){//Gym Leader's pokemon attacks
                //Print out info
                System.out.println(fight.getName()+" use "+fight.getMove());
                System.out.println(player.pokemonData.getName()+" received "+fight.getAttack()+" damages.");
                //Player's pokemon get attacked, damage caused
                player.pokemonData.damaged(fight.getAttack());
                if(player.pokemonData.getCurrentHP()<=0){
                    //The player lost the game
                    System.out.println(player.pokemonData.getName()+" fainted.");
                    System.out.println("You lost the battle, and all of your money as well.");
                    //Lost all the money
                    player.changeMoney(0);
                    //Game over
                    break;
                }
                System.out.println(player.pokemonData.getName()+" has "+player.pokemonData.getCurrentHP()+" HP left.");

            }
            else {//Player's pokemon attacks
                System.out.println(player.pokemonData.getName()+" use "+player.pokemonData.getMove());
                System.out.println(fight.getName()+" received "+player.pokemonData.getAttack()+" damages.");
                //Gym leader's pokemon got attacked, damage caused
                fight.damaged(player.pokemonData.getAttack());
                if(fight.getCurrentHP()<=0){
                    //Gym leader lost the game
                    System.out.println(fight.getName()+" fainted.");
                    System.out.println("Congrate! You won!");
                    // Get money from the gym leader , randomly from 100 to 150 dollars
                    Integer winmoney = random.nextInt(51)+100;
                    System.out.println("You won "+winmoney+" dollars.");
                    player.changeMoney(winmoney);
                    //Get the badge
                    System.out.println("You got one "+badge+"!");
                    player.acceptbadge(badge);
                    //Print out pokemon info
                    System.out.println(player.pokemonData.getName()+" has "+player.pokemonData.getCurrentHP()+" HP left.");
                    break;
                }
                System.out.println(fight.getName()+" has "+fight.getCurrentHP()+" HP left.");
            }
            //Change Terms
            attack = !attack;
        }
        scanner.nextLine();
        //Gym leader's pokemon's hp get charged
        fight.chargeHP();
    }
}
