package ss.labs.pokemon.character;
import ss.labs.pokemon.PokemonData;
import java.util.HashMap;

/**
 * Created by yen ping on 2017/3/13.
 */
public class Player extends PokemonTrainer{

    private Integer money;
    private HashMap<String,Integer> badges = new HashMap<>();//Use to store the badges player got

    public Player(String name, String job, String quote, PokemonData pokemon){
        super(name,job,quote,pokemon);
        this.money = 30;
    }
    //Either lost all the money(change = 0) or gain 'change'
    public void changeMoney(Integer change){
        if(change!=0)
            money = money+change;
        else
            money=0;
    }
    public Integer pokemonhp(){
        return pokemonData.getCurrentHP();
    }
    public void acceptbadge(String badgename){
        if(!badges.containsKey(badgename)){
            badges.put(badgename,1);
        }
        else {
            Integer number = badges.get(badgename);
            badges.put(badgename,number+1);
        }
    }
    public Integer getmoney(){
        return money;
    }
    // show all the badges player won
    public void showBadge(){
        for (String name:badges.keySet())
            System.out.println(name+" : "+badges.get(name));
    }
}
