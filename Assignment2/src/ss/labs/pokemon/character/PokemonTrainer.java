package ss.labs.pokemon.character;
import ss.labs.pokemon.PokemonData;
import java.util.Random;
import java.util.Scanner;

/**
 * Created by jerry on 2017/3/7.
 */
public class PokemonTrainer extends NPC{
    protected PokemonData pokemonData;

    //TODO solve the error
    public PokemonTrainer(String name, String job, String quote, PokemonData data)
    {
        //TODO call super constructor
        super(name,job,quote);
        //TODO assign attributes
        pokemonData = data;
    }

    public void showInformation(){
        super.showInformation();
    }
    //TODO override toString()
    public String toString(){
        return super.toString()+pokemonData;
    }

    public void interact(Player player){
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();
        //If attack is true, pokemon trainer go first
        //else player go first
        boolean attack = (pokemonData.getSpeed()>player.pokemonData.getSpeed());
        System.out.println("Let the battle begin......");
        while(true){
            // Push enter to continue the game
            scanner.nextLine();
            if(attack){//pokemon trainer's pokemon attacks
                //Print info
                System.out.println(pokemonData.getName()+" use "+pokemonData.getMove());
                System.out.println(player.pokemonData.getName()+" received "+pokemonData.getAttack()+" damages.");
                //player's pokemon gets attacked, damage caused
                player.pokemonData.damaged(pokemonData.getAttack());
                if(player.pokemonData.getCurrentHP()<=0){
                    //Player lost
                    System.out.println(player.pokemonData.getName()+" fainted.");
                    System.out.println("You lost the battle, and all of your money as well.");
                    //lost money
                    player.changeMoney(0);
                    //game over
                    break;
                }
                System.out.println(player.pokemonData.getName()+" has "+player.pokemonData.getCurrentHP()+" HP left.");
            }
            else {//player's pokemon attacks
                System.out.println(player.pokemonData.getName()+" use "+player.pokemonData.getMove());
                System.out.println(pokemonData.getName()+" received "+player.pokemonData.getAttack()+" damages.");
                //pokemon trainer's pokemon gets attacked, damaged caused
                pokemonData.damaged(player.pokemonData.getAttack());
                if(pokemonData.getCurrentHP()<=0){
                    // pokemon trainer lost
                    System.out.println(pokemonData.getName()+" fainted.");
                    System.out.println("Congrate! You won!");
                    // Player gets money from pokemon trainer, from 50 to 110 dollars.
                    Integer winmoney = random.nextInt(61)+50;
                    System.out.println("You won "+winmoney+" dollars.");
                    player.changeMoney(winmoney);
                    //print out player's pokemon info
                    System.out.println(player.pokemonData.getName()+" has "+player.pokemonData.getCurrentHP()+" HP left.");
                    break;
                }
                System.out.println(pokemonData.getName()+" has "+pokemonData.getCurrentHP()+" HP left.");
            }
            //Change Terms
            attack = !attack;
        }
        scanner.nextLine();
        //Pokemon Trainer's pokemon's hp gets charged automatically
        pokemonData.chargeHP();
    }
}
