package ss.labs.pokemon.character;

import ss.labs.pokemon.PokemonData;

import java.util.ArrayList;

/**
 * Created by jerry on 2017/3/7.
 * In this practice you will see the power of polymorphism
 * practice to inherit and
 */
public class NPCGenerator {
    //no modifier is private
    private ArrayList<NPC> npcTemplates;
    public NPCGenerator()
    {
        npcTemplates = new ArrayList<>();
        //TODO create some NPC and put into the template list. At least one for each type of character
        Villagers Villager = new Villagers("Michael","miller","Hi! How are you!");
        Villagers Villager1 = new Villagers("Beck","chief","Hi! How are you!");
        Villagers Villager2 = new Villagers("Angelica","banker","Hi! How are you!");
        Villagers Villager3 = new Villagers("Elizabeth","farmer","Hi! How are you!");
        Villagers Villager4 = new Villagers("Melissa","teacher","Hi! How are you!");
        Villagers Villager5 = new Villagers("Lee","musician","Hi! How are you!");
        Villagers Villager6 = new Villagers("Luara","comedian","Hi! How are you!");

        PokemonData pokemon = new PokemonData("Pikachu","Electric","Thunderbolt",180,166,103);
        PokemonTrainer player = new PokemonTrainer("Hillary","Pokemon Trainer","Let's battle!!!",pokemon);
        PokemonData pokemon1 = new PokemonData("Magikarp","Water","Splash",150,148,22);
        PokemonTrainer player1 = new PokemonTrainer("Lin","Pokemon Trainer","Let's battle!!!",pokemon1);
        PokemonData pokemon2 = new PokemonData("Charmander","Fire","Ember",188,121,98);
        PokemonTrainer player2 = new PokemonTrainer("Chris","Pokemon Trainer","Let's battle!!!",pokemon2);
        PokemonData pokemon3 = new PokemonData("Squirtle","Water","Water Gun",198,81,90);
        PokemonTrainer player3 = new PokemonTrainer("Bill","Pokemon Trainer","Let's battle!!!",pokemon3);
        PokemonData pokemon4 = new PokemonData("Chikorita","Grass","Thunderbolt",200,85,92);
        PokemonTrainer player4 = new PokemonTrainer("Kristen","Pokemon Trainer","Let's battle!!!",pokemon4);
        PokemonData pokemon5 = new PokemonData("Growlithe","Fire","Flame Wheel",220,112,130);
        PokemonTrainer player5 = new PokemonTrainer("Kate","Pokemon Trainer","Let's battle!!!",pokemon5);
        PokemonData pokemon6 = new PokemonData("Onix","Rock","Rock Throw",180,130,85);
        PokemonTrainer player6 = new PokemonTrainer("George","Pokemon Trainer","Let's battle!!!",pokemon6);

        Nurse nurse = new Nurse("Joy","Can I help you?");
        Nurse nurse1 = new Nurse("Joy","Can I help you?");
        Nurse nurse2 = new Nurse("Joy","Can I help you?");
        Nurse nurse3 = new Nurse("Joy","Can I help you?");
        Nurse nurse4 = new Nurse("Joy","Can I help you?");
        Nurse nurse5 = new Nurse("Joy","Can I help you?");

        PokemonData pokemon7 = new PokemonData("Gasty","Ghost","Spite",170,148,67);
        PokemonData pokemon8 = new PokemonData("Gengar","Ghost","Shadow Punch",230,202,121);
        PokemonData pokemon9 = new PokemonData("Seel","Water","Water Sport",240,85,85);
        PokemonData pokemon10 = new PokemonData("Dewgong","Water","Icy Wind",290,130,130);
        PokemonData pokemon11 = new PokemonData("Gyarados","Water","Dragpn Rage",300,150,229);
        PokemonData pokemon12 = new PokemonData("Charizard","Fire","Dragon Claw",266,184,155);
        PokemonData pokemon13 = new PokemonData("Voltorb","Electric","Spark",190,184,58);
        PokemonData pokemon14 = new PokemonData("Magnemite","Electric","Thunder Shock",160,85,67);
        PokemonData pokemon15 = new PokemonData("Onix","Rock","Rock Throw",180,130,85);
        PokemonData pokemon16 = new PokemonData("Cranidos","Rock","Rock Throw",244,108,229);
        GymLeader leader = new GymLeader("Morty","Gym Leader","Good of you to have come here, in Ecruteak.",pokemon7,pokemon8,"Fog Badge");
        GymLeader leader1 = new GymLeader("Pryce","Gym Leader"," I have been with Pokémon since before you were born. I do not lose easily.",pokemon9,pokemon10,"Glacier Badge");
        GymLeader leader2 = new GymLeader("Clair","Gym Leader","I am Clair. The world's best dragon master. I can hold my own against even the Pokémon League's Elite Four. Do you still want to take me on? ...Fine. Let's do it! ",pokemon11,pokemon12,"Rising Badge");
        GymLeader leader3 = new GymLeader("Wattson","Gym Leader","Now, what are you doing here? What's that? You say you've gotten past all my rigged doors? Wahahahah! Now, that is amusing!",pokemon13,pokemon14,"Dynamo Badge");
        GymLeader leader4 = new GymLeader("Roark","Gym Leader","Welcome! This is the Oreburgh Pokémon Gym! I'm Roark, the Gym Leader! I'm but one Trainer who decided to walk proudly with Rock-type Pokémon!",pokemon15,pokemon16,"Coal Badge");

        npcTemplates.add(nurse);
        npcTemplates.add(nurse1);
        npcTemplates.add(nurse2);
        npcTemplates.add(nurse3);
        npcTemplates.add(nurse4);
        npcTemplates.add(nurse5);
        npcTemplates.add(Villager);
        npcTemplates.add(Villager1);
        npcTemplates.add(Villager2);
        npcTemplates.add(Villager3);
        npcTemplates.add(Villager4);
        npcTemplates.add(Villager5);
        npcTemplates.add(Villager6);
        npcTemplates.add(player);
        npcTemplates.add(player1);
        npcTemplates.add(player2);
        npcTemplates.add(player3);
        npcTemplates.add(player4);
        npcTemplates.add(player5);
        npcTemplates.add(player6);
        npcTemplates.add(leader);
        npcTemplates.add(leader1);
        npcTemplates.add(leader2);
        npcTemplates.add(leader3);
        npcTemplates.add(leader4);
    }

    //Try ctrl-N or command-N, click on Getter, you will see something useful
    //TODO write a public method to return npctemplates
    public ArrayList<NPC> getNpcTemplates() {
        return npcTemplates;
    }
}
