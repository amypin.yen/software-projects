package ss.labs.pokemon.character;
import java.util.Random;

/**
 * Created by yen ping on 2017/3/13.
 */
public class Villagers extends NPC{

    public Villagers(String name,String job,String quote){
        super(name,job,quote);
    }

    public void interact(Player player){
        Random random = new Random();
        Integer givemoney = random.nextInt(30)+1;
        speak();

        System.out.println(name+" : Thanks for taking to me. Here's "+givemoney+" dollars.");
        player.changeMoney(givemoney);
    }



}
