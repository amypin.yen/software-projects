package ss.labs.pokemon.character;

/**
 * Created by yen ping on 2017/3/10.
 */
public class Nurse extends NPC{

    public Nurse(String name, String quote){
        super(name, "Nurse",quote);
    }

    public void interact(Player player){
        speak();

        player.pokemonData.chargeHP();//charge pokemon's hp

        System.out.println("Your pokemon now has full hp! Here you go!");

    }

}
