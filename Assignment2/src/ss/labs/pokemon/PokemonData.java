package ss.labs.pokemon;

/**
 * Created by jerry on 2017/3/7.
 */
public class PokemonData{
    public PokemonData(String name,String type,String move,Integer HP,Integer speed,Integer attack){
        this.name = name;
        this.type = type;
        this.move = move;
        this.maxHP = HP;
        this.currentHP = HP;
        this.speed = speed;
        this.attack = attack;
    }

    private String name;
    private String type;
    private String move;
    private Integer maxHP;
    private Integer currentHP;
    private Integer speed;
    private Integer attack;


    //Try ctrl-N or command-N, click on Getter, you will see something useful

    //TODO implement "get function" for name
    public String getName(){
        return name;
    }
    //TODO implement "get function" for type
    public String getType(){
        return type;
    }

    public String getMove(){return move;}

    public Integer getSpeed(){return speed;}

    public Integer getCurrentHP(){return currentHP;}

    public Integer getAttack(){return attack;}

    //Pokemon gets attacked, damage caused
    public Integer damaged(Integer damage){
        currentHP = currentHP-damage;
        return currentHP;
    }
    //Pokemon's hp charge
    public void chargeHP(){currentHP = maxHP;}



}
