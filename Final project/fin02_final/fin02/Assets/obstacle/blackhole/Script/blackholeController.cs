﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class blackholeController : MonoBehaviour {

	float Speed;
	public GameObject airplane;
    
	// Use this for initialization
	void Start () {
		Speed = 2;
	}
	
	// Update is called once per frame
	void Update () {
		if(AirplaneController.isblackhole){
			Speed = 0.5f;
		}
		else{
			Speed = 3;
			
		}
		transform.Translate(Vector2.down * Speed * Time.deltaTime);
		if (GetComponent<Transform>().position.y <-11.11){
			Destroy(this.gameObject);
		}
	}
	void OnTriggerEnter2D(Collider2D item)
    {
        if (item.gameObject.tag == "Player"){
            airplane.GetComponent<AirplaneController>().blackholecollide();
            
        }
    }
}
