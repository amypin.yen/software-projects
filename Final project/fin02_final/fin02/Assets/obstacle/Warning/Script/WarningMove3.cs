﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarningMove3 : MonoBehaviour
{
    public GameObject getObject;
    public Vector3 _target;
    public float minPosX;
    public float maxPosX;
    private float _speed;
    private bool _move;
    public float time;
    public float disappeartime;
    public bool iswarn;
    public static bool isHourglass;
    public GameObject fireball;
    private GameObject childGameObject;
    public GameObject firedownaudio;
    // Use this for initialization
    void Start()
    {
        iswarn = true;
    }

    // Update is called once per frame
    void Update()
    {

        
        _target.x = transform.position.x;
        _target.z = transform.position.z;
        _target.y = transform.position.y;
        _target.x = Mathf.Clamp(_target.x, minPosX, maxPosX);
        _speed = 5;
        transform.position = Vector3.MoveTowards(transform.position, _target, _speed * Time.deltaTime);
        time += 1;
        disappeartime += 1;
        if (time > 180 && iswarn == true)
        {
            gameObject.SetActive(false);
            iswarn = false;
            time = 0;
            childGameObject = Instantiate(fireball);
            Instantiate(firedownaudio, Vector2.zero, Quaternion.identity);
            childGameObject.transform.localPosition = _target;
            childGameObject.AddComponent<NullScript>();
            Destroy(this.gameObject);
        }


    }
}

