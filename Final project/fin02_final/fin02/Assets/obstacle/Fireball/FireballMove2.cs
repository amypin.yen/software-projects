﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FireballMove2 : MonoBehaviour
{

    public int PlanetSpeed;
    public int life;
    public TextMesh PlanetText;
    Animator anim;
    public GameObject drop;
    public bool exist;
    public GameObject getObject;


    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (GetComponent<RectTransform>().position.y < -5)
            Destroy(this.gameObject);


        PlanetText.text = "" + life;

        if (life == 0)
        {
            //exist = false;
            for (int i = 5; i > 0; i--)
            {
                Instantiate(drop, GetComponent<RectTransform>().position, new Quaternion(0, 0, 0, 0));
            }
            anim.SetBool("toexplore", true);
            Destroy(GetComponent<CircleCollider2D>());
            Destroy(PlanetText);
        }
        else if (GetComponent<RectTransform>().position.y < -5)
            Destroy(this.gameObject);
        else
        {
            anim.SetBool("toexplore", false);
        }


    }

    void OnTriggerEnter2D(Collider2D Coll)
    {
        if (Coll.gameObject.tag == "star")
        {
            if (life > 0) life--;
        }

    }


}
