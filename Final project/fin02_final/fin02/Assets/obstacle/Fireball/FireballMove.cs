﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FireballMove : MonoBehaviour
{

    float PlanetSpeed;
    float forcevalue;
    public int life;
    public TextMesh PlanetText;
    Animator anim;
    public GameObject drop;
    public bool exist;
    public GameObject getObject;


    void Start()
    {
        PlanetSpeed = 10;
        forcevalue = 0;
    }

    // Update is called once per frame
    void Update()
    {
       
        if (GetComponent<RectTransform>().position.y <-11.11)
            Destroy(this.gameObject);
             
        
        PlanetText.text = "" + life;
       
        if (life == 0)
        {
            //exist = false;
            for (int i = 5; i > 0; i--)
            {
                Instantiate(drop, GetComponent<RectTransform>().position, new Quaternion(0, 0, 0, 0));
            }
            anim.SetBool("toexplore", true);
            Destroy(GetComponent<CircleCollider2D>());
            Destroy(PlanetText);
        }
        else if (GetComponent<RectTransform>().position.y < -16)
            Destroy(this.gameObject);
        

    }

    void OnTriggerEnter2D(Collider2D Coll)
    {
        if (Coll.gameObject.tag == "star")
        {
            if (life > 0) life--;
        }
        
    }


}
