﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlanetMove : MonoBehaviour {

    float PlanetSpeed;
    float forcevalue;
    public int life;
    public TextMesh PlanetText;
    Animator anim;
    public GameObject drop;
    public GameObject airplane;
    public GameObject ranbowstar;
    public GameObject Hourglass;
    public GameObject bulletaudio;
    public GameObject exploreaudio;
    public GameObject oil;
    public GameObject heart;
    public GameObject magnet;
    List<GameObject> speciallist;
    bool hadexplore;
    
  
    void Start()
    {
        anim = GetComponent<Animator>();
        PlanetSpeed = 5;
        forcevalue = 0;
        speciallist = new List<GameObject>();
        speciallist.Add(ranbowstar);
        speciallist.Add(Hourglass);
        speciallist.Add(oil);
        speciallist.Add(heart);
        speciallist.Add(magnet);
        hadexplore = false;
    }

    // Update is called once per frame
    void Update()
    {
        forcevalue += 0.1f;
        transform.Translate(Vector2.down * PlanetSpeed * Time.deltaTime);
        if(AirplaneController.isflame){
           PlanetSpeed = 25;
        }
        else if(AirplaneController.isHourglass){
            PlanetSpeed = 2;
        }
        else{
            PlanetSpeed = 5 + forcevalue;
        }
        if(life!=0){
            PlanetText.text = "" + life;
        }
        if (GetComponent<RectTransform>().position.y <-12){
             Destroy(this.gameObject);
        }
        if ((life == 0) && !hadexplore)
        {
            Instantiate(exploreaudio, Vector2.zero, Quaternion.identity);
            hadexplore = true;
            for(int i=5;i>0;i--){
                Instantiate(drop, GetComponent<RectTransform>().position, new Quaternion(0, 0, 0, 0));
            }
            int ifdrop = Random.Range(0, 100);
            if (ifdrop > 80)    //the probability of the drop of special items 
            {
                int choose = Random.Range(0, speciallist.Count);
                Instantiate(speciallist[choose], GetComponent<RectTransform>().position, new Quaternion(0, 0, 0, 0));
            }
            anim.SetBool("toexplore", true);
            Destroy(GetComponent<CircleCollider2D>());
            Destroy(PlanetText);
        }
        
    }
    
    void OnTriggerEnter2D(Collider2D Coll)
    {
        
        if (Coll.gameObject.tag == "star") 
        {
            if(life>0)life--;
            Instantiate(bulletaudio, Vector2.zero,Quaternion.identity);
        }
        else if(Coll.gameObject.tag == "bigstar")
        {
            if(life>0){
                life = life-2;
                if(life<0)life = 0;
            }
            Instantiate(bulletaudio, Vector2.zero, Quaternion.identity);
        }
        else if(Coll.gameObject.tag == "laser")
        {
            //exist = false;
            for(int i=5;i>0;i--){
                Instantiate(drop, GetComponent<RectTransform>().position, new Quaternion(0, 0, 0, 0));
            }
            life = 0;
            Instantiate(exploreaudio, Vector2.zero, Quaternion.identity);
            anim.SetBool("toexplore", true);
            Destroy(GetComponent<CircleCollider2D>());
            Destroy(PlanetText);  
        }
    }

  
}
