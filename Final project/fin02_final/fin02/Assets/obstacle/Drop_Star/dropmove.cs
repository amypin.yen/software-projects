﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dropmove : MonoBehaviour {
    private Rigidbody2D rigidbody2D = null;
    public float forceValue;
    public GameObject getObject;
    Vector2 force2D = Vector2.zero;
    // Use this for initialization
    void Start () {
        rigidbody2D = this.GetComponent<Rigidbody2D>();
        float y_add = Random.Range(0, 2f);            
        force2D.y = force2D.y + y_add;

	}
	
	// Update is called once per frame
	void Update () {
        
        if (force2D.x == 0)  //limit the x's increment
        {
            float x_add = Random.Range(-1f, 1f);            
            force2D.x = force2D.x + x_add;
        }
 
        rigidbody2D.AddForce(force2D);


        //transform.Translate(Vector2.up *dropspeed*Time.deltaTime);
        
        //transform.position = new Vector2(transform.position.x-vx0, transform.position.y+vy0);
        //vy0 = vy0 + dropspeed;
        if (GetComponent<Transform>().position.y <-12)
            Destroy(this.gameObject);
    }
    void OnTriggerEnter2D(Collider2D item)
    {
        if (item.gameObject.tag == "Player"){
            
            Destroy(this.gameObject);

        }
    }
}
