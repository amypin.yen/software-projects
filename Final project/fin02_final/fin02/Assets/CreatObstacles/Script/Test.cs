﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour
{
    public GameObject Planet1;
    public GameObject Planet2;
    public GameObject Planet3;
    public GameObject Planet4;
    public GameObject SpecialPlanet;
    public GameObject fireball;
    public GameObject Warning;
    public GameObject Warning2;
    public GameObject Warning3;
    public GameObject blackhole;
    public GameObject warningaudio;   //warning sound
     

    public float PlanetTime;
    public GameObject copyGameObject;//要被複製的物件
    public GameObject superGameObject;//要被放置在哪個物件底下
    private Vector3 randomPosition;
    public List<GameObject> listobject_1;
    public List<GameObject> listobject_2;
    public List<GameObject> listobject_3;
    public List<GameObject> listobject_4;
    public List<GameObject> listobject_5;
    public List<GameObject> listobject_6;
    public List<GameObject> listobject_7;
    public List<GameObject> listobject_8;
    private GameObject childGameObject;//被複製出來的物件
    private double posx1,posx2,posx3,posx4,posx5,posx0,posx6;
    private List<double> posx;
    public GameObject getObject1;
    public GameObject getObject;
    public bool warningoccur;
    private bool havewarning;

    //files
    int counter = 0;
    float dtime;
    System.IO.StreamReader file;
    System.IO.StreamReader file1;



    

    void Start()
    {
       
        file = new System.IO.StreamReader("easy.txt");
        posx0 = -4.69;
        posx1= -3.1;
        posx2= -1.51;
        posx4 = 0.05;
        posx3 = 1.64;
        posx5 = 3.2;
        posx6 = 4.74;
       
    }


    void Update()
    {
        
        FindWarning iswarn;
        iswarn = getObject1.GetComponent<FindWarning>();
        string line;
        int height = getObject.GetComponent<AirplaneController>().ReturnHeight();
        bool isHourglass = AirplaneController.isHourglass;
        bool isflame = AirplaneController.isflame;
        int temp_difftime = UnityEngine.Random.Range(0, 4);
        
        if (temp_difftime == 0) dtime = 0.2f;
        else if (temp_difftime == 1) dtime = 0.5f;
        else if (temp_difftime == 2) dtime = 0.7f;
        else if (temp_difftime == 3) dtime = 0.9f;
        else if (temp_difftime == 4) dtime = 1.1f;
        float difftime;
        if (isHourglass) difftime = 0.9f+dtime;
        else if (isflame) difftime = dtime;
        else
        {
            difftime = 0.5f+dtime;
        }
        
        PlanetTime += Time.deltaTime;
        randomPosition.y = 15;
        randomPosition.z = 0;

        if ( file.EndOfStream == true)
        {
            int temp_file = UnityEngine.Random.Range(0, 3);
            if (temp_file==0) file = new System.IO.StreamReader("test1.txt");
            else if (temp_file == 1) file = new System.IO.StreamReader("test2.txt");
            else if (temp_file == 2) file = new System.IO.StreamReader("test3.txt");
            else if (temp_file == 3) file = new System.IO.StreamReader("test4.txt");
        }
        
        if (PlanetTime > difftime && (line = file.ReadLine()) != null)
        {
            PlanetTime = 0;
            counter++;
            for (int number = 0; number < 7; number++)
            {
                if (line[number] == '1')
                {
                    if (number == 0) randomPosition.x = -4.69f;
                    else if (number == 1) randomPosition.x = -3.1f;
                    else if (number == 2) randomPosition.x = -1.51f;
                    else if (number == 3) randomPosition.x = 0.05f;
                    else if (number == 4) randomPosition.x = 1.64f;
                    else if (number == 5) randomPosition.x = 3.2f;
                    else randomPosition.x = 4.74f;
                    copyGameObject = ReturnRandom(height);

                    childGameObject = Instantiate(copyGameObject);
                    childGameObject.transform.localPosition = randomPosition;
                    childGameObject.AddComponent<NullScript>();
                }
                else if (line[number] == '0')
                {

                }
            }




            //fireball
            int temp_fireball;
            if(0 <= height && height < 100000)
                temp_fireball = UnityEngine.Random.Range(0, 9);
            else if (100000<= height && height < 200000)
                temp_fireball = UnityEngine.Random.Range(0, 8);
            else if (200000 <= height && height < 400000)
                temp_fireball = UnityEngine.Random.Range(0, 7);
            else
                temp_fireball = UnityEngine.Random.Range(0, 6);

            
            if (temp_fireball == 0 && iswarn.ReturnWarning() == 0)
            {
                randomPosition.y = 9;
                randomPosition.x = getObject.GetComponent<Transform>().position.x;
                childGameObject = Instantiate(Warning);
                Instantiate(warningaudio, Vector2.zero, Quaternion.identity);
                childGameObject.transform.localPosition = randomPosition;
                childGameObject.AddComponent<NullScript>();
                childGameObject.transform.parent = superGameObject.transform;
            }
            else if (temp_fireball == 0 && iswarn.ReturnWarning() == 1)
            {
                randomPosition.y = 9;
                randomPosition.x = getObject.GetComponent<Transform>().position.x;
                childGameObject = Instantiate(Warning2);
                Instantiate(warningaudio, Vector2.zero, Quaternion.identity);
                childGameObject.transform.localPosition = randomPosition;
                childGameObject.AddComponent<NullScript>();
                childGameObject.transform.parent = superGameObject.transform;
            }
            else if (temp_fireball == 0 && iswarn.ReturnWarning() > 1)
            {
                randomPosition.y = 9;
                int temp_fireballpos = UnityEngine.Random.Range(0, 6);
                if (temp_fireballpos == 0) randomPosition.x = (float)posx1;
                else if (temp_fireballpos == 1) randomPosition.x = (float)posx2;
                else if (temp_fireballpos == 2) randomPosition.x = (float)posx3;
                else if (temp_fireballpos == 3) randomPosition.x = (float)posx4;
                else if (temp_fireballpos == 4) randomPosition.x = (float)posx0;
                else if (temp_fireballpos == 5) randomPosition.x = (float)posx5;
                else randomPosition.x = (float)posx6;
                Instantiate(warningaudio, Vector2.zero, Quaternion.identity);
                childGameObject = Instantiate(Warning3);
                childGameObject.transform.localPosition = randomPosition;
                childGameObject.AddComponent<NullScript>();
                childGameObject.transform.parent = superGameObject.transform;
            }
            //blackhole
            int temp_blackhole = UnityEngine.Random.Range(0, 40);
            if (temp_blackhole == 0)
            {
                randomPosition.y = 9;
                int temp_blackholepos = UnityEngine.Random.Range(0, 6);
                if (temp_blackholepos == 0) randomPosition.x = (float)posx1;
                else if (temp_blackholepos == 1) randomPosition.x = (float)posx2;
                else if (temp_blackholepos == 2) randomPosition.x = (float)posx3;
                else if (temp_blackholepos == 3) randomPosition.x = (float)posx4;
                else if (temp_blackholepos == 4) randomPosition.x = (float)posx0;
                else if (temp_blackholepos == 5) randomPosition.x = (float)posx5;
                else randomPosition.x = (float)posx6;

                childGameObject = Instantiate(blackhole);
                
                childGameObject.transform.localPosition = randomPosition;
                childGameObject.AddComponent<NullScript>();

            }

        }
        
        
    }

    private GameObject ReturnRandom(int height)
    {


        if (0 <= height && height < 100000)
        {
            int temp_pos = UnityEngine.Random.Range(0, 16);
            if (0 <= temp_pos && temp_pos <= 10) return Planet1;
            else if (11 <= temp_pos && temp_pos <= 12) return Planet2;
            else if (12 <= temp_pos && temp_pos <= 13) return Planet3;
            else if (temp_pos == 14) return Planet4;
            else if (temp_pos == 15) return SpecialPlanet;
            else return null;
        }
        else if (100000<= height && height < 200000)
        {
            int temp_pos = UnityEngine.Random.Range(0, 16);
            if (0 <= temp_pos && temp_pos <= 4) return Planet1;
            else if (5 <= temp_pos && temp_pos <= 9) return Planet2;
            else if (10 <= temp_pos && temp_pos <= 12) return Planet3;
            else if (13 <= temp_pos && temp_pos <= 14) return Planet4;
            else if (temp_pos == 15) return SpecialPlanet;
            else return null;
        }
        else if (200000 <= height && height < 400000)
        {
            int temp_pos = UnityEngine.Random.Range(0, 16);
            if (0 <= temp_pos && temp_pos <= 2) return Planet1;
            else if (3 <= temp_pos && temp_pos <= 6) return Planet2;
            else if (7 <= temp_pos && temp_pos <= 10) return Planet3;
            else if (11 <= temp_pos && temp_pos <= 13) return Planet4;
            else if (temp_pos == 14 || temp_pos == 15) return SpecialPlanet;
            else return null;
        }
        else
        {
            int temp_pos = UnityEngine.Random.Range(0, 16);
            if (0 <= temp_pos && temp_pos <= 2) return Planet1;
            else if (3 <= temp_pos && temp_pos <= 6) return Planet2;
            else if (7 <= temp_pos && temp_pos <= 10) return Planet3;
            else if (11 <= temp_pos && temp_pos <= 13) return Planet4;
            else if (temp_pos == 14 || temp_pos == 15) return SpecialPlanet;
            else return null;
        }
        

    }
}


