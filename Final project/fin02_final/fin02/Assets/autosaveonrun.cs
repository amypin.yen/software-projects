﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
//using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;
[InitializeOnLoad]
public class autosaveonrun : ScriptableObject
{

	// Use this for initialization
	
    static autosaveonrun()
    {
        EditorApplication.playmodeStateChanged = () =>
        {
            if (EditorApplication.isPlayingOrWillChangePlaymode && !EditorApplication.isPlaying)
            {
                //Debug.Log("Auto-Saving scene before entering Play mode: " + EditorApplication.currentScene);
                if (EditorSceneManager.SaveOpenScenes())
                {
                    Debug.Log("Scene auto saved");
                }
                //EditorApplication.SaveScene();
                AssetDatabase.SaveAssets();
            }
        };
    }
}
    #endif


