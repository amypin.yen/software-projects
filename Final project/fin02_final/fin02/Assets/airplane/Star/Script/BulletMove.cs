﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMove : MonoBehaviour {

	// Use this for initialization
    public int BulletSpeed = 50;
   
    
   
    void Start () {
	}
	
	// Update is called once per frame
	void Update () {

        transform.Translate(Vector2.up *BulletSpeed* Time.deltaTime);      
        if (GetComponent<Transform>().position.y >10)
            Destroy(this.gameObject);
       
	}
    
    void OnTriggerEnter2D(Collider2D Coll)
    {
          
          if (Coll.tag == "planet")
          {
             Destroy(this.gameObject); //刪除碰撞到的物件(CubeA)
          }
    }
}
