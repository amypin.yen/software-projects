﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour {

	// Use this for initialization
    public GameObject Bullet;
    public GameObject BigBullet;
    public GameObject getobject;
    
    public float BulletTime;
    public int StarsNumbers;
    bool islaser;
    bool isblackhole;
    bool recover;
    public Sprite red;
    public Sprite green;
    public Sprite blue;
    public Vector3 star1pos;
    public Vector3 star2pos;
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        islaser = getobject.GetComponent<AirplaneController>().islaser;
        recover = MouseMove.recover;
        isblackhole = AirplaneController.isblackhole;
        BulletTime += Time.deltaTime;
        
        if(getobject.GetComponent<SpriteRenderer>().sprite == red ){
            if (BulletTime > 0.2f && StarsNumbers>0 && !islaser &&!recover &&!isblackhole)
            {
                BulletTime = 0;
                StarsNumbers = StarsNumbers - 1;
                Instantiate(Bullet, transform.position, new Quaternion(0, 0, 0, 0));
            }
        }
        else if(getobject.GetComponent<SpriteRenderer>().sprite == green ){
            if (BulletTime > 0.4f && StarsNumbers>0 && !islaser &&!recover &&!isblackhole)
            {
                BulletTime = 0;
                StarsNumbers = StarsNumbers - 2;
                if (StarsNumbers < 0) StarsNumbers = 0;
                 star1pos = transform.position;
                star2pos = transform.position;
                star1pos.x = transform.position.x -0.5f;
                star2pos.x = transform.position.x +0.5f;
                Instantiate(Bullet, star1pos, new Quaternion(0, 0, 0, 0));
                Instantiate(Bullet, star2pos, new Quaternion(0, 0, 0, 0));
            }
        }
        else{
            if (BulletTime > 0.6f && StarsNumbers>0 && !islaser &&!recover &&!isblackhole)
            {
                BulletTime = 0;
                StarsNumbers = StarsNumbers - 1;
                Instantiate(BigBullet, transform.position, new Quaternion(0, 0, 0, 0));
            }
        }



	}

    public int ReturnStarsNumbers()
    {
        return StarsNumbers;
    }
    public void addbullet()
    {
        StarsNumbers += 1;
    }
    public void minusbullet()
    {
        StarsNumbers = StarsNumbers - 1;
    }
    public void reloadbullet()
    {
        if(StarsNumbers <50){
            StarsNumbers = 50; 
        }
    }
}
