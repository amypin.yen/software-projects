﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class AirplaneController : MonoBehaviour {


    public TextMesh BulletText;
    public TextMesh Height;
    public TextMesh hearttext;
    public int BulletNumber = 100;
    public static int h = 0;
    public int playerlife = 1;
    public static AirplaneController Instance;
    public GameObject getObject;
    public GameObject laser;
    public GameObject flame;
    public GameObject eataudio;
    public GameObject fireaudio;
    public GameObject laseraudio;
    public GameObject blackholeaudio;
    public bool islaser;
    public static bool isflame;
    public static bool isHourglass;
    public static bool isblackhole;
    public float lasertime;
    public float flametime;
    public float Hourglasstime;
    public float blackholetime;
    public static bool flameclose;

    public GameObject circle_magnet;
    public static bool isMagnet;
    public float magnettime;
    Animator anim;

    public Vector3 _target;

    void Start()
    {
        Instance = this;
        islaser = false;
        isflame = false;
        isHourglass = false;
        isMagnet = false;
        isblackhole = false;
        flameclose = false;
        //laser.gameObject.SetActive(false);
        flame.gameObject.SetActive(false);
        circle_magnet.gameObject.SetActive(false);
        lasertime = 0;
        flametime = 0;
        Hourglasstime = 0;
        magnettime = 0;
        blackholetime = 0;
        anim = GetComponent<Animator>();
        h = 0;
    }

    // Update is called once per frame
   
    
    void Update()
    {
        h = (isflame)? h+100 : h+10;
        //h=h+10;
        // if(BulletNumber>0)BulletNumber = BulletNumber-1;
        Shooting shot;
        shot = getObject.GetComponent<Shooting>();
        BulletText.text = "" + shot.ReturnStarsNumbers();
        Height.text = "" + h/100+"."+h%100 + " M";
        hearttext.text = "X "+playerlife;
        if(islaser){
            lasertime += 1;
            if(lasertime > 360){
                 //laser.gameObject.SetActive(false);
                 islaser = false;
                 lasertime = 0;
            }
        }
        if(isflame){
            flametime += 1;
            if(flametime > 360){
                 flame.gameObject.SetActive(false);
                 isflame = false;
                 flametime = 0;
                 GetComponent<PolygonCollider2D>().isTrigger = false;
            }
        }
        if(isHourglass){
            Hourglasstime += 1;
            if(Hourglasstime > 360){
                 isHourglass = false;
                 Hourglasstime = 0;
            }
        }
        if (isMagnet)
        {
            magnettime += 1;
            if (magnettime > 360)
            {
                isMagnet = false;
                magnettime = 0;
                circle_magnet.gameObject.SetActive(false);
            }
        }
        if(isblackhole){
            //laser.gameObject.SetActive(false);
            islaser = false;
            circle_magnet.gameObject.SetActive(false);
            anim.SetBool("isblackhole",true);
            GetComponent<PolygonCollider2D>().isTrigger = true;
            blackholetime += 1;
            if(blackholetime > 120){
                isblackhole = false;
                anim.SetBool("isblackhole",false);
                blackholetime = 0;
                transform.position = new Vector3(0.07f, -12, 0);
            }
        }
    }

    public int ReturnHeight()
    {
        return h;
    }
    public void minuslife(){
        playerlife -= 1;
    }
    void OnTriggerEnter2D(Collider2D item)
    {
        if (!MouseMove.recover && !isblackhole)
        {
            if (item.gameObject.tag == "rainbowstar")
            {
                if (islaser == false)
                {
                    _target = transform.position;
                    _target.y = transform.position.y + 8.5f;
                    Instantiate(laser, _target, new Quaternion(0, 0, 0, 0));
                }
                lasertime = 0;
                islaser = true;
                //laser.gameObject.SetActive(true);
               
                Instantiate(eataudio, Vector2.zero, Quaternion.identity);   //eat sound
                Instantiate(laseraudio, Vector2.zero, Quaternion.identity);   //laser sound
            }
            if (item.gameObject.tag == "dropstar")
            {
                getObject.GetComponent<Shooting>().addbullet();
            }
            if (item.gameObject.tag == "oil")
            {
                if (isHourglass)
                {
                    isHourglass = false;
                    Hourglasstime = 0;
                }
                flameclose = false;
                flametime = 0;
                flame.gameObject.SetActive(true);
                isflame = true;
                Instantiate(fireaudio, Vector2.zero, Quaternion.identity);
                GetComponent<PolygonCollider2D>().isTrigger = true;
                Instantiate(eataudio, Vector2.zero, Quaternion.identity);   //eat sound
            }
            if (item.gameObject.tag == "Hourglass")
            {
                if (isflame)
                {
                    isflame = false;
                    flametime = 0;
                    flameclose = true;
                    flame.gameObject.SetActive(false);
                    GetComponent<PolygonCollider2D>().isTrigger = false;
                }
                Hourglasstime = 0;
                isHourglass = true;
                Instantiate(eataudio, Vector2.zero, Quaternion.identity);   //eat sound
            }
            if (item.gameObject.tag == "heart")
            {
                playerlife += 1;
                Instantiate(eataudio, Vector2.zero, Quaternion.identity);   //eat sound
            }
            if (item.gameObject.tag == "Magnet")
            {
                isMagnet = true;
                magnettime = 0;
                circle_magnet.gameObject.SetActive(true);
                Instantiate(eataudio, Vector2.zero, Quaternion.identity);   //eat sound
            }
        }
    }
    
    public void blackholecollide()
    {
        if(!isblackhole && !MouseMove.recover &&!isflame){
            isblackhole = true;
            Instantiate(blackholeaudio, Vector2.zero, Quaternion.identity);
            islaser = false;
            lasertime = 0;
            isMagnet = false;
            magnettime = 0;
            isHourglass = false;
            Hourglasstime = 0;
        }
    }

    public float returnflametime()
    {
        return flametime;
    }
    public bool returnflametimeclose()
    {
        return flameclose;
    }
    public float returnlasertime()
    {
        return lasertime;
    }

}
