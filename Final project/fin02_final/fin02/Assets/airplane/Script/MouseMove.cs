﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseMove : MonoBehaviour
{

    public float _speed;
    private bool _move;
    public GameObject _point;
    public Vector3 _target;
    public float minPosX;
    public float maxPosX;
    Animator anim;
    public GameObject laser;
    public GameObject flame;
    public GameObject circle_magnet;
    public static bool recover;
    public static bool gameover;
    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();
        recover = false;
        gameover = false;
    }

    // Update is called once per frame
    void Update()
    {

            if(AirplaneController.isblackhole){
                    transform.position = transform.position;
            }
            else if(recover && (GetComponent<Transform>().position.y < -4.57) ){
                _target = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                _target.z = transform.position.z;
                _target.y = transform.position.y + (float)0.05;
                _target.x = Mathf.Clamp(_target.x, minPosX, maxPosX);
                transform.position = Vector3.Lerp(transform.position, _target,10);
            }
            else if (GetComponent<Transform>().position.y < -4.57 && GetComponent<Transform>().position.y > -11)
            {
                _target = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                _target.z = transform.position.z;
                _target.y = transform.position.y + (float)0.03;
                _target.x = Mathf.Clamp(_target.x, minPosX, maxPosX);
                transform.position = Vector3.Lerp(transform.position, _target,10);  
            }
            else if (GetComponent<Transform>().position.y < -11)
            {
               
                if(GetComponent<AirplaneController>().playerlife>0){
                    recover = true;
                    //laser.gameObject.SetActive(false);
                    circle_magnet.gameObject.SetActive(false);
                    GetComponent<AirplaneController>().islaser = false;
                    AirplaneController.isMagnet = false;
                    GetComponent<AirplaneController>().lasertime = 0;
                    GetComponent<AirplaneController>().magnettime = 0;
                    anim.SetBool("isdead",true);
                    GetComponent<PolygonCollider2D>().isTrigger = true;
                    GetComponent<AirplaneController>().minuslife();
                    GetComponent<Shooting>().reloadbullet();
                }
                else{
                    gameover = true;
                }
            }
            else
            {
                if(recover == true){
                    recover = false;
                    anim.SetBool("isdead",false);
                    GetComponent<PolygonCollider2D>().isTrigger = false;
                }
                _target = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                _target.z = transform.position.z;
                _target.y = transform.position.y;

                _target.x = Mathf.Clamp(_target.x, minPosX, maxPosX);
                transform.position = Vector3.MoveTowards(transform.position, _target, _speed * Time.deltaTime);
            }
        


       
  
    }

   
}
