﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagnetController : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}

    void Update()
    {
        if (GetComponent<Transform>().position.y < -11.1)
        {
            Destroy(this.gameObject);
        }
    }
    void OnTriggerEnter2D(Collider2D item)
    {
        if (item.gameObject.tag == "Player" || item.gameObject.tag == "Circle_M")
        {
            Destroy(this.gameObject);
        }
    }
}
