﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class laserComtroller : MonoBehaviour {

	public GameObject plane;
    private Vector3 _target;
	// Use this for initialization
    public float minPosX;
    public float maxPosX;
    public float _speed;
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        plane = GameObject.FindGameObjectWithTag("Player");
        AirplaneController airplanecontroller = plane.GetComponent<AirplaneController>();
        _target = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        _target.z = transform.position.z;
        _target.y = plane.GetComponent<Transform>().position.y + 8.5f ;

        _target.x = Mathf.Clamp(_target.x, minPosX, maxPosX);
        transform.position = Vector3.MoveTowards(transform.position, _target, _speed * Time.deltaTime);

        if (airplanecontroller.lasertime == 360 || AirplaneController.isblackhole||MouseMove.recover)
        {
            Destroy(this.gameObject);
        }
		
	}
	void OnTriggerEnter2D(Collider2D item)
    {
        /*if (item.gameObject.tag == "dropstar")
        {
            plane.GetComponent<Shooting>(). minusbullet();
        }*/
    }
}
