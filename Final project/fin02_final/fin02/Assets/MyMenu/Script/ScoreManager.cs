﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ScoreManager : MonoBehaviour {

   // public String score;
    public static double count;
    public GameObject winaudio;
    public GameObject failaudio;
    Text txt;
	// Use this for initialization
	void Start () {
        if (GameOverManager.rate >=1 && GameOverManager.rate <=3&&MouseMove.gameover)
        {
            Instantiate(winaudio,Vector2.zero,Quaternion.identity);
        }
        else if (GameOverManager.rate != 0 && MouseMove.gameover)
        {
            Instantiate(failaudio, Vector2.zero, Quaternion.identity);
        }
        count = 0;
        txt = gameObject.GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
        int h = AirplaneController.h;
        int hieght = h / 100; 
        if (count + hieght/20 <= hieght)
            count = count + hieght / 20;
        else
            count = hieght;
        txt.text = count.ToString("R")+"M";
        if (GameOverManager.rate == 1)
        {
            txt.color = new Color(251f/255f,1f,0f);
        }
        else if (GameOverManager.rate == 2)
        {
            txt.color = new Color(1f, 1f, 1f,237f/255f);
        }
        else if (GameOverManager.rate == 3)
        {
            txt.color = new Color(251f / 255f, 160f/255f, 0f);
        }
	}
}
