﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEnd : MonoBehaviour {
    public static bool onlyonce;

	// Use this for initialization
	void Start () {
        onlyonce = false;
	}
	
	// Update is called once per frame
	void Update () {
        if (!onlyonce)
        {
            if (MouseMove.gameover)
            {
                onlyonce = true;
                MouseMove.gameover = false;
                GetComponent<PanelTransistor>().EndGame = true;
            }
            
        }
	}
}
