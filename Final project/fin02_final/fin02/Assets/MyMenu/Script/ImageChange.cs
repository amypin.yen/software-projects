﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImageChange : MonoBehaviour {

	// Use this for initialization
	void Start () {
        if(SaveImage.img != null)
            GetComponent<SpriteRenderer>().sprite = SaveImage.img;
	}
	
	// Update is called once per frame
	void Update () {
         
    }
}
