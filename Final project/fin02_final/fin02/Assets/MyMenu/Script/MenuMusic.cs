﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuMusic : MonoBehaviour {

    AudioSource fxSound;
    public AudioClip backMusic;
    public Canvas can;
    public Menu MainMenu;
    // Use this for initialization
    void Start()
    {

        fxSound = GetComponent<AudioSource>();
        if(can.GetComponent<MenuManager>().CurrentMenu==MainMenu)
            fxSound.Play();
    }

    // Update is called once per frame
    void Update()
    {
        if (can.GetComponent<MenuManager>().CurrentMenu == MainMenu&&!fxSound.isPlaying)
            fxSound.Play();
    }
}
