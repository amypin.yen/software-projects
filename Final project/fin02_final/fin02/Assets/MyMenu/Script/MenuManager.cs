﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuManager : MonoBehaviour {

    public Menu CurrentMenu;
    public Menu MainMenu;
    public Menu ScoreMenu;

    public void Start()
    {
        if (MouseMove.gameover)
        {
            CurrentMenu = ScoreMenu;
            GameEnd.onlyonce = false;
        }
        else
        {
            CurrentMenu = MainMenu;
        }

        ShowMenu(CurrentMenu);
    }


    public void ShowMenu(Menu menu)
    {
        if (CurrentMenu != null)
            CurrentMenu.IsOpen = false;

        CurrentMenu = menu;
        CurrentMenu.IsOpen = true;
    }
}
