﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelTransistor : Menu{


    public void Start()
    {
        EndTransform = false;
        OpenAnimation = true;
    }


    public bool EndGame
    {
        get { return _animator.GetBool("EndGame"); }
        set { _animator.SetBool("EndGame", value); }
    }

    public bool EndTransform
    {
        get { return _animator.GetBool("EndTransform"); }
        set { _animator.SetBool("EndTransform", value); }
    }

    public bool OpenAnimation
    {
        get { return _animator.GetBool("OpenAnimation"); }
        set { _animator.SetBool("OpenAnimation", value); }
    }

    public void SetEndAnimation()
    {
        this.EndGame  = false;
    }
    public void SetTransform()
    {
        this.EndTransform = true;
    }
    public void SetOpenAnimation()
    {
        this.OpenAnimation = false;
    }
    public void SetOrigin()
    {
        EndTransform = false;
        OpenAnimation = true;
    }
    public void SetTrue()
    {
        EndGame = true;
    }

}
