﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonMusic : MonoBehaviour {


    public void playAudio(GameObject buttonAudio)
    {
        Instantiate(buttonAudio, Vector2.zero, Quaternion.identity);
    }
}
