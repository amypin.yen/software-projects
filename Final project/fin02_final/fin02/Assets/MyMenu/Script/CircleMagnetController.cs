﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleMagnetController : MonoBehaviour
{

    public GameObject plane;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
    }
    void OnTriggerStay2D(Collider2D item)
    {
        if (item.gameObject.tag == "dropstar")
        {
            item.GetComponent<Rigidbody2D>().AddForce((plane.GetComponent<Transform>().position - item.GetComponent<Transform>().position).normalized * 5000000 * Time.smoothDeltaTime);
        }
    }
    void OnTriggerEnter2D(Collider2D item)
    {
        if (item.gameObject.tag == "dropstar")
        {
            plane.GetComponent<Shooting>(). minusbullet();
        }
    }
}
