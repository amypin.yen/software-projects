﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroyflame : MonoBehaviour {
    public float audiotime;
    public GameObject airplane;
    void Start()
    {
        audiotime = 0;

    }

    // Update is called once per frame
    void Update()
    {
        airplane = GameObject.FindGameObjectWithTag("Player");
        AirplaneController airplanecontroller = airplane.GetComponent<AirplaneController>();
        if (airplanecontroller.returnflametime() == 1)
            audiotime = 1;
        audiotime = audiotime + 1;
        if (audiotime > 360|| airplanecontroller.returnflametimeclose())
        {
            Destroy(this.gameObject);
        }
    }
}
