package com.sslab.pokemon.guicomponent;

import com.sslab.pokemon.PokemonSprite;

import javax.swing.*;
import java.nio.file.Watchable;
import java.util.Random;


/**
 * Created by jerry on 2017/3/26.
 */
public class PokemonLabel extends JLabel{
    private Integer id;

    public PokemonLabel()
    {
        setIcon(PokemonSprite.bushIcon());
        id = -1;
    }

    public void Update()
    {
        //TODO setup a counter, if time up you may want to change to another state
        Random random = new Random();
        if (state == WhacPokemonState.Caught)//Prevent being in Caught state too long because of the countdown
            hidePokemon();
        Integer counter = random.nextInt(40000000);
        while (!counter.equals(0))
            counter = counter-1;
        if (random.nextBoolean())
            popPokemon();
        else
            hidePokemon();

    }

    public void popPokemon(){
        //TODO when a pokemon pop up
        Random random = new Random();
        state = WhacPokemonState.Show;
        Integer num = random.nextInt(7)%4;
        if (num <3) {//first three type in the species
            switch (random.nextInt(5)) {//random species
                case 0://Bulbasaur and same species
                    id = num;
                    break;
                case 1://Charmander and same species
                    id = num + 4;
                    break;
                case 2://Squirtl  and same species
                    id = num + 9;
                    break;
                case 3:// Pikachu or Diglett  or Dugtrio
                    if (num.equals(1))
                        id = 33;
                    else
                        id = num +62;
                    break;
                case 4://Gastly and same species
                    id = num + 116;
            }
        }
        else {//other 6  pokemons
            switch (random.nextInt(6)){
                case 0://Mega Venusaur
                    id = 3;
                    break;
                case 1://Mega Charizard X
                    id = 7;
                    break;
                case 2://Mega Blastoise
                    id = 12;
                    break;
                case 3://Chikorita
                    id = 185;
                    break;
                case 4://Cyndaquil
                    id = 188;
                    break;
                case 5://Totodile
                    id = 191;
            }
        }
        ImageIcon  poke = new ImageIcon(PokemonSprite.getSprite(id));
        setIcon(poke);

    }
    public void hidePokemon()
    {
        //TODO when the pokemon hide into the bushes
        state = WhacPokemonState.Hide;
        setIcon(PokemonSprite.bushIcon());
        id = -1;
    }
    public void caught()
    {
        //TODO when player caught the pokemon
        state = WhacPokemonState.Caught;
        setIcon(PokemonSprite.pokeballIcon());
    }

    public boolean isCatchable()
    {
        //a beautiful way to write it, no need to use if
        return state == WhacPokemonState.Show;
    }
    WhacPokemonState state = WhacPokemonState.Hide;

    public Integer getId(){
        return id;
    }
}
enum WhacPokemonState{
    Hide,Show,Caught
}
