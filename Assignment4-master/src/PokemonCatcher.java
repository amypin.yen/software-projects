import com.sslab.pokemon.GameThread;
import com.sslab.pokemon.guicomponent.BGPanel;
import com.sslab.pokemon.guicomponent.PokemonLabel;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

/**
 * Created by jerry on 2017/3/26.
 */
public class PokemonCatcher {
    GameThread gameThread;
    private JPanel root;
    private JLabel pokemon1;
    private JPanel bgPanel;
    private JLabel pokemon2;
    private JLabel pokemon3;
    private JLabel pokemon4;
    private JLabel pokemon5;
    private JLabel scoreLabel;

    ArrayList<PokemonLabel> pokemons;
    int score=0;
    public PokemonCatcher() {
        HashMap<String,Integer> map = new HashMap<>();
        map.put("Bulb",0);
        map.put("Char",0);
        map.put("Squi",0);

        MouseAdapter listener = new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);

                //force cast and get pokemon from the event component
                PokemonLabel pokemon = (PokemonLabel) e.getComponent();

                //TODO see if pokemon is catchable, update the UI with SwingUtilities.invokeLater
                if(pokemon.isCatchable()){
                    pokemon.caught();
                    Integer id = pokemon.getId();
                    if (id.equals(3)){
                        Integer count = map.get("Bulb");
                        map.put("Bulb",count+1);
                    }
                    else if (id.equals(7)){
                        Integer count = map.get("Char");
                        map.put("Char",count+1);
                    }
                    else if (id.equals(12)){
                        Integer count = map.get("Squi");
                        map.put("Squi",count+1);
                    }
                    else if (id<3){
                        Integer exp = 1;
                        for (int i =1 ;i <=map.get("Bulb");i++)
                            exp = exp*2;
                        score = score+(id+1)*exp;
                    }
                    else if (id>3&&id<7){
                        Integer exp = 1;
                        for (int i =1 ;i <=map.get("Char");i++)
                            exp = exp*2;
                        score = score+(id-3)*exp;
                    }
                    else if (id>8&&id<12){
                        Integer exp = 1;
                        for (int i =1 ;i <=map.get("Squi");i++)
                            exp = exp*2;
                        score = score+(id-8)*exp;
                    }
                    else if (id.equals(62)||id.equals(64)){
                        score = score-1;
                    }
                    else if (id.equals(33)){
                        if (score>0)
                            score = score/2;
                        else
                            score = score*2;
                    }
                    else if (id>115&&id<119){
                        Random random = new Random();
                        Integer sub = (random.nextInt(10)+1)*20;
                        score = score-sub;
                    }
                    else if (id.equals(118)){
                        map.put("Bulb",0);
                        map.put("Char",0);
                        map.put("Squi",0);
                    }
                    else{
                        if (score<0)
                            score = score/2;
                        else
                            score = score*2;
                    }

                }
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        scoreLabel.setText("" + score);
                    }
                });
            }
        };

        //add mouse listeners to all the pokemonLabels
        for(JLabel pokemon:pokemons)
        {
            pokemon.addMouseListener(listener);
        }

        //create the game thread,
        gameThread = new GameThread(pokemons);
    }
    public static void main(String[] args) {
        JFrame frame = new JFrame("PokemonCatcher");
        frame.setContentPane(new PokemonCatcher().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setLayout(null);
    }


    private void createUIComponents() {
        BufferedImage img = null;

        try {
            img = ImageIO.read(new File("res/bg.png"));
        } catch (IOException e) {
        }
        bgPanel = new BGPanel(img);
        pokemons = new ArrayList<>();

        for(int i=0;i<5;i++)
            pokemons.add(new PokemonLabel());
        pokemon1 = pokemons.get(0);
        pokemon2 = pokemons.get(1);
        pokemon3 = pokemons.get(2);
        pokemon4 = pokemons.get(3);
        pokemon5 = pokemons.get(4);

    }
}
