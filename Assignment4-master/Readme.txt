104062322 嚴心平

實作
GameThread
	每個update 之後休息1300ms
PokemonCatcher
	判斷完分數後，運用
		SwingUtilities.invokeLater 
	updates score
	增加 HashMap<String, Integer> map, 用以紀錄抓到的 Mega Pokemon(Use for my own rule)
PokemonLabel
	利用兩個random使出現的Pokemon機率不同，結果如下
		普通加減分Pokemons
			Bulbasaur,Ivysaur,Venusaur
			Charmander,Charmeleon,Charizard    
			Squirtle,Wartortle,Blastoise			出現機率 2/35
			Pikachu,Diglett,Dugtrio
			Gastly,Haunter,Gengar
			
		特殊加分Pokemons
			Mega Venusaur,Mega Charizard,Mega Blastoise		出現機率 1/42
			Chikorita,Cyndaquil,Totodile
			
Rules Design
	普通加分Pokemons
		Bulbasaur,Charmander,Squirtle	加1分
		Ivysaur,Charmeleon,Wartortle	加2分	(此處為原始加分)
		Venusaur,Charizard,Blastoise	加3分
	特別加分Pokemons
		1. Mega Venusaur,Mega Charizard,Mega Blastoise		
		累計抓到的Mega Pokemon 加分為(原始分數*2^(同species Mega Pokemon 抓到個數))
		ex:
			累計抓到的Mega Venusaur 為3，然後抓到一隻Bulbasaur
				score+=1*2^(3)=8
		2. 抓到新御三家 Chikorita,Cyndaquil,Totodile
			分數變成兩倍
	減分Pokemons
		原本的Diglett,Dugtrio	減1分
		被Pikachu電到	score變為1/2
		抓到Gastly,Haunter,Gengar	扣20 ~ 200之間分數
			
遇到的困難與解決
	本來並無設counter 在PokemonLabel Update(),因為差距並無很大，後來嘗試多次才底定random值。
	分數要用到exponential的計算，上網找了資料後還是決定用最原始的方式，
		原因數字並不會多大加上Math.pow型別是double的關係出現了一些error
		