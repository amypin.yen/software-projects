104062322 嚴心平

實作
一、PokemonIndividualData 
含有三個值
	nickname
	comboID  <--用來和SpeciesID作區別
	value   <--為PokemonValueData類別
除了getNickname(),get.....用來回傳，增加了
	clean() <--當delete某個pokemon時，用clean()將其歸零
二、PokemonSpeciesData 稍作改變
	新增了 nickname，以作儲存
三、Pokedex的addNewPokemon
	新增nickname
	將speciesValue從int[]改為PokemonValueData
四、PokeGen
	1.使用statFields ArrayList 儲存六項數值+nickname
	2.將所有slot在剛始就加進PokemonMap
	3.初始畫面選擇slot0,全部的數值為0(包括其他5個slots)
	4.input 的數值只有在切換slot或按下save時，才會被儲存到PokemonMap
	5.按下save後PokemonMap中的資料會被存為json file
	6.為了避免error停止了program，在此使用try  catch防止使用者輸入不符合規定的input，並跳出視窗提醒
	7.不符合規定的input主要是，要輸入數字的六項指標輸入的不是數字
		如果沒有取nickname是可被接受的，仍可執行並存檔
		如果沒有在combobox選擇pokemon種類，所輸入的input仍會被儲存到pokemonMap(仍可以在使用介面看到input)
			，但如按下save並不會被存到json file
	8.對視窗介面有作修改
		移動slot位置
		將代表slot的JPanel作最小值設定
		將Button的高加寬
	9.六個slot皆使用同個Listener，使用get Component判斷是哪個slot
