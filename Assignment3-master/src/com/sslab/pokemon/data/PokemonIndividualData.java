package com.sslab.pokemon.data;

/**
 * Created by jerry on 2017/3/21.
 */
public class PokemonIndividualData {
    //TODO create variables and constructor for this class
    private String nickname;
    private Integer comboID;
    private PokemonValueData value;

    public PokemonIndividualData(String name,Integer id,int[] value){
        this.nickname = name;
        this.comboID = id;
        this.value = new PokemonValueData(value);
    }

    public String getNickname(){
        return nickname;
    }
    public Integer getcomboID(){
        return comboID;
    }
    public PokemonValueData getValue(){
        return  value;
    }
    public void clean(){
        int[] values = {0,0,0,0,0,0};
        PokemonValueData newvalue = new PokemonValueData(values);
        this.nickname = "";
        this.comboID = 0;
        this.value = newvalue;
    }
}
