package com.sslab.pokemon.data;

/**
 * Created by jerry on 2017/3/1.
 */

public class PokemonSpeciesData// implements Comparable<PokemonSpeciesData>
{
    private int id;
    private String speciesName;
    private String nickname;
    private PokemonValueData speciesValue;


    //HashMap<Integer,MoveData> learnMoveTable;
    public PokemonSpeciesData(int id, String speciesName,PokemonValueData valueData,String nick)
    {
        this.id = id;
        this.speciesName = speciesName;
        this.speciesValue = valueData;
        this.nickname = nick;
    }

    public int getId() {
        return id;
    }

    public String getSpeciesName() {
        return speciesName;
    }

    public PokemonValueData getSpeciesValue() {
        return speciesValue;
    }

    public String getNickname(){
        return nickname;
    }

}

//https://wiki.52poke.com/wiki/种族值