package com.sslab.pokemon;

import com.sslab.pokemon.data.PokemonIndividualData;
import com.sslab.pokemon.sprite.PokemonSprite;

import javax.swing.*;
import java.awt.event.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by jerry on 2017/3/19.
 */
public class PokeGen {
    private JComboBox speciesComboBox;
    private JPanel root;

    private JButton deleteButton;
    private JButton saveButton;

    private JPanel slot0;
    private JPanel slot1;
    private JPanel slot2;
    private JPanel slot3;
    private JPanel slot4;
    private JPanel slot5;
    private JTextField nickNameField;
    private JTextField hpField;
    private JTextField atkField;
    private JTextField defField;
    private JTextField spAtkField;
    private JTextField spDefField;
    private JTextField speedField;
    private JPanel currentSelectedPanel;
    private ArrayList<JTextField> statFields = new ArrayList<>();

    Pokedex pokedex;
    HashMap<JPanel, PokemonIndividualData> pokemonMap = new HashMap<>();

    public PokeGen() {
        //TODO:load file
        pokedex = new Pokedex("bin/pokemonData.json");
        // TODO:add to combobox
        speciesComboBox.addItem("--------");
        for (int i=1;i< pokedex.getPokemonSize();i++){
            speciesComboBox.addItem(i+":"+pokedex.getPokemonData(i-1).getSpeciesName());
        }
        slot0.setBorder(BorderFactory.createBevelBorder(1));
        slot1.setBorder(BorderFactory.createBevelBorder(0));
        slot2.setBorder(BorderFactory.createBevelBorder(0));
        slot3.setBorder(BorderFactory.createBevelBorder(0));
        slot4.setBorder(BorderFactory.createBevelBorder(0));
        slot5.setBorder(BorderFactory.createBevelBorder(0));
        int[] value = {0,0,0,0,0,0};
        PokemonIndividualData poke = new PokemonIndividualData("",-1,value);
        pokemonMap.put(slot3,poke);
        pokemonMap.put(slot4,poke);
        pokemonMap.put(slot2,poke);
        pokemonMap.put(slot1,poke);
        pokemonMap.put(slot5,poke);
        pokemonMap.put(slot0,poke);

        statFields.add(nickNameField);
        statFields.add(hpField);
        statFields.add(atkField);
        statFields.add(defField);
        statFields.add(spAtkField);
        statFields.add(spDefField);
        statFields.add(speedField);
        currentSelectedPanel = slot0;
        loadPokemon(currentSelectedPanel);
        deleteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                PokemonIndividualData poke = pokemonMap.get(currentSelectedPanel);
                poke.clean();
                pokemonMap.put(currentSelectedPanel,poke);
                loadPokemon(currentSelectedPanel);
                speciesComboBox.setSelectedIndex(0);
            }
        });
        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Pokedex pokepake = new Pokedex();
                setPokemon(currentSelectedPanel);
                for (JPanel key:pokemonMap.keySet()) {
                    PokemonIndividualData poke = pokemonMap.get(key);
                    if (poke.getcomboID()>0)
                        pokepake.addNewPokemon(pokedex.getPokemonData(poke.getcomboID()-1).getId(), pokedex.getPokemonData(poke.getcomboID()).getSpeciesName(), poke.getValue(), poke.getNickname());
                }
                try {
                    pokepake.saveFile("morris_new_pokemon.json");
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });
        speciesComboBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setPokemonIcon(speciesComboBox.getSelectedIndex()-1,(JLabel)currentSelectedPanel.getComponent(0));
            }
        });
        MouseAdapter listener = new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                JPanel panel = (JPanel)e.getComponent();
                if (currentSelectedPanel != panel){
                    setPokemon(currentSelectedPanel);
                    currentSelectedPanel.setBorder(BorderFactory.createBevelBorder(0));
                    currentSelectedPanel = panel;
                    loadPokemon(currentSelectedPanel);
                }
                currentSelectedPanel.setBorder(BorderFactory.createBevelBorder(1));
            }
        };
        slot0.addMouseListener(listener);
        slot1.addMouseListener(listener);
        slot2.addMouseListener(listener);
        slot3.addMouseListener(listener);
        slot4.addMouseListener(listener);
        slot5.addMouseListener(listener);
    }

    public void setPokemon(JPanel panel) {
        int[] value = new int[6];
        for (int i =0;i<6;i++){
            Integer text=0;
            //Prevent error stops the program, and print out message
            try {
                value[i] = text.parseInt(statFields.get(i + 1).getText());
            }catch(NumberFormatException nfe){
                JFrame pop = new JFrame();
                JOptionPane.showMessageDialog(pop,"StatFieds have not number inputs which would not be saved.");
            }
        }
        PokemonIndividualData newpoke = new PokemonIndividualData(statFields.get(0).getText(),speciesComboBox.getSelectedIndex(),value);
        pokemonMap.put(panel,newpoke);

    }

    public void loadPokemon(JPanel panel) {
        int[] value = {0,0,0,0,0,0};
        PokemonIndividualData poke = pokemonMap.get(panel);
        if(poke.getcomboID()==-1)
            speciesComboBox.setSelectedIndex(0);
        else
            speciesComboBox.setSelectedIndex(poke.getcomboID());
        statFields.get(0).setText(poke.getNickname());
        value = poke.getValue().getValArray();
        for(int i=1;i<7;i++){
            statFields.get(i).setText(""+value[i-1]);
        }
    }
    private void setPokemonIcon(int id,JLabel label)
    {
        ImageIcon icon;
        if (id!= -1)
            icon = new ImageIcon(PokemonSprite.getSprite(id));
        else
            icon = new ImageIcon("");
        label.setIcon(icon);
    }
    public static void main(String[] args) {
        JFrame frame = new JFrame("PokeGen");
        frame.setContentPane(new PokeGen().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

}